const config = require('./../config.json');
const axios = require('axios');
const Discord = require('discord.js');
const Canvas = require('canvas');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(process.env.DATABASE_URL || config.heroku_postgres_remote);
const Users = require('./../models/users.js')(sequelize, Sequelize.DataTypes);

Canvas.registerFont('./assets/fonts/Play-Regular.ttf', { family: 'Play', weigth: 'normal' });
Canvas.registerFont('./assets/fonts/Play-Bold.ttf', { family: 'Play', weigth: 'bold' });

module.exports = {
	name: 'elo',
	description: 'get elo',
	async execute(message, args) {
		const user_id = message.author.id;

		const user = await Users.findOne({
			where: { user_id: user_id },
		});

		if (!user) {
			message.channel.send('Не указан ник :< . Юзай /set_nickname <nickname>');
		}
		else {
			axios.get(`${config.faceit_api}players?nickname=${user.faceit_nickname}`, { 'headers': { 'Authorization': 'Bearer ' + config.faceit_api_key } })
				.then(async (response) => {
					const canvas = Canvas.createCanvas(700, 140);
					const ctx = canvas.getContext('2d');
					ctx.fillStyle = '#232828';
					ctx.fillRect(0, 0, canvas.width, canvas.height);

					// drawAvatar
					if (response.data.avatar !== '') {
						const avatar = await Canvas.loadImage(response.data.avatar);
						ctx.drawImage(avatar, 20, 20, 100, 100);
					}
					ctx.lineWidth = 4;
					ctx.strokeStyle = '#202225';
					ctx.beginPath();
					ctx.moveTo(20, 20);
					ctx.lineTo(120, 20);
					ctx.lineTo(120, 120);
					ctx.lineTo(20, 120);
					ctx.lineTo(20, 20);
					ctx.lineTo(20, 30);
					ctx.lineJoin = 'miter';
					ctx.lineJoin = 'round';
					ctx.stroke();
					ctx.lineWidth = 1;

					// drawLogo
					const logo = await Canvas.loadImage('./assets/images/faceit-logo.png');
					ctx.drawImage(logo, 570, 20, 100, 30);

					// drawNickname
					ctx.font = 'bold 21px Play';
					ctx.fillStyle = '#EBEFF3';
					ctx.fillText(user.faceit_nickname, 140, 40);

					// drawinfractions
					ctx.font = 'normal 14px Play';
					ctx.fillStyle = '#EBEFF3';
					ctx.fillText(`Нарушения: afk:${response.data.infractions.afk} leaver: ${response.data.infractions.leaver}`, 140, 70);

					// drawEloInfo
					ctx.font = 'normal 16px Play';
					ctx.fillStyle = '#EBEFF3';
					ctx.fillText(`${response.data.games.csgo.faceit_elo} ELO - LVL:${response.data.games.csgo.skill_level}`, 140, 100);
					const attachment = new Discord.MessageAttachment(canvas.toBuffer(), 'face-info.png');

					// send msg with attachment
					message.channel.send(`${user.faceit_nickname} stats:`, attachment);
				})
				.catch((error) => {
					if (error.response !== undefined && error.response.status === 404) {
						message.channel.send(`Не найден пользователь с ником ${user.faceit_nickname}. Юзай /set_nickname <nickname>`);
					}
					else {
						console.log(error);
					}
				});
		}
	},
};
