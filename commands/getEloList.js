const config = require('./../config.json');
const axios = require('axios');
const Discord = require('discord.js');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(process.env.DATABASE_URL || config.heroku_postgres_remote);
const Users = require('./../models/users.js')(sequelize, Sequelize.DataTypes);

module.exports = {
	name: 'elo_list',
	description: 'get list users [name, elo]',
	async execute(message, args) {
		const users = await Users.findAll();
		const eloList = [];
		for (let user of users) {
			const userInfoResponse = await axios.get(`${config.faceit_api}players?nickname=${user.faceit_nickname}`, { 'headers': { 'Authorization': 'Bearer ' + config.faceit_api_key } })
			// messageContent += `${user.faceit_nickname} elo: ${userInfoResponse.data.games.csgo.faceit_elo}\n`;
			eloList.push({
				nick_name: user.faceit_nickname,
				faceit_elo: userInfoResponse.data.games.csgo.faceit_elo,
			});
		}

		eloList.sort((a, b) => (a.faceit_elo > b.faceit_elo) ? -1 : 1);

		message.channel.send(eloList.map((el, i) => {
			let emoji;
			if (i === 0) {
				emoji = '<:smert_2:552491799353098250>';
			}
			else if (i === 1) {
				emoji = '<:humonoid_m:552456633117310996>';
			}
			else if (i === 2) {
				emoji = '<:hl:552493328180969483>';
			}
			else {
				emoji = '<:tnn:552480143814164480>';
			}
			return `${emoji} ${el.nick_name} elo: ${el.faceit_elo}`;
		}));
	},
};
