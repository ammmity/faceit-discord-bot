const config = require('./../config.json');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(process.env.DATABASE_URL || config.heroku_postgres_remote);
const Users = require('./../models/users.js')(sequelize, Sequelize.DataTypes);

module.exports = {
	name: 'set_nickname',
	description: 'attach faceit nickname to user',
	async execute(message, args) {
		const user_id = message.author.id;
		const nickname = message.content.split(`${config.bot_command_prefix}${this.name} `)[1];

		const user = await Users.findOne({
			where: { user_id: user_id },
		});

		if (!user) {
			await Users.create({
				user_id: user_id,
				faceit_nickname: nickname,
				faceit_subscribe_to_match_ending: 0,
			});
		}
		else {
			await user.update({ faceit_nickname: nickname });
		}

		message.channel.send('Ник успешно привязан');
	},
};
