const config = require('./config.json');
const fs = require('fs');
const Discord = require('discord.js');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(process.env.DATABASE_URL || config.heroku_postgres_remote);

if (process.env.NODE_ENV === 'development') {
	process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
}

// require sequilize models
const Users = require('./models/users.js')(sequelize, Sequelize.DataTypes);

const client = new Discord.Client();
client.commands = new Discord.Collection();

client.once('ready', () => {
	Users.sync();

	console.log('Ready!');
});

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
	const command = require(`./commands/${file}`);

	// set a new item in the Collection
	// with the key as the command name and the value as the exported module
	client.commands.set(command.name, command);
}

client.on('message', message => {
	if (!message.content.startsWith(config.bot_command_prefix) || message.author.bot) return;

	const args = message.content.slice(config.bot_command_prefix.length).trim().split(/ +/);
	const command = args.shift().toLowerCase();

	if (command === 'set_nickname') {
		client.commands.get('set_nickname').execute(message, args);
	}
	else if (command === 'elo') {
		client.commands.get('elo').execute(message, args);
	}
	else if (command === 'elo_list') {
		client.commands.get('elo_list').execute(message, args);
	}
});

client.login(config.bot_token);
