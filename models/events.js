module.exports = (sequelize, DataTypes) => {
	return sequelize.define('events', {
		match_id: {
			type: DataTypes.STRING,
			primaryKey: true,
		},
	}, {
		timestamps: true,
	});
};
