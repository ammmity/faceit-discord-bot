const config = require('./config.json');
const axios = require('axios');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const Discord = require('discord.js');
const Canvas = require('canvas');
const util = require('util');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(process.env.DATABASE_URL || config.heroku_postgres_remote);
// require sequilize models
const Events = require('./models/events.js')(sequelize, Sequelize.DataTypes);

if (process.env.NODE_ENV === 'development') {
	process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;
}

Canvas.registerFont('./assets/fonts/Play-Regular.ttf', { family: 'Play', weigth: 'normal' });
Canvas.registerFont('./assets/fonts/Play-Bold.ttf', { family: 'Play', weigth: 'bold' });

const client = new Discord.Client();
client.login(config.bot_token);

Events.sync();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


client.on('ready', () => {
	app.get('/', (req, res) => {
		res.end(';)');
	});

	app.post('/faceit-match-end', async (req, res) => {
		// console.log(util.inspect(req.body, { showHidden: false, depth: null }));
		// console.log(util.inspect(req.query, { showHidden: false, depth: null }));
	
		const matchId = req.body.payload.id;
	
		console.log('ID матча: ' + matchId);
	
		if (!matchId) {
			res.end(';)');
			return;
		}
	
		const match = await Events.findOne({
			where: { match_id: matchId },
		});
	
		if (!match) {
			// add event in db to prevent multiple handling same events
			await Events.create({
				match_id: matchId,
			});
	
			try {
				const matchInfoResponse = await axios.get(`${config.faceit_api}matches/${matchId}`, { 'headers': { 'Authorization': 'Bearer ' + config.faceit_api_key } });
				const matchStatsResponse = await axios.get(`${config.faceit_api}matches/${matchId}/stats`, { 'headers': { 'Authorization': 'Bearer ' + config.faceit_api_key } });
				const matchInfo = matchInfoResponse.data;
				const matchStats = matchStatsResponse.data;
	
				const canvas = Canvas.createCanvas(700, 465);
				const ctx = canvas.getContext('2d');
				ctx.fillStyle = '#232828';
				ctx.fillRect(0, 0, canvas.width, canvas.height);
	
				// drawHeader
				const mapName = matchInfo.voting.map.pick[0];
				const mapImageSrc = (matchInfo.voting.map.entities.filter(map => map.game_map_id === mapName))[0].image_sm;
				const mapImage = await Canvas.loadImage(mapImageSrc);
				ctx.drawImage(mapImage, 20, 20, 110, 55);
	
				ctx.font = 'bold 21px Play';
				ctx.fillStyle = '#EBEFF3';
				const headerTitle = matchStats.rounds[0].teams[0].team_stats.Team +
					' ' + matchStats.rounds[0].teams[0].team_stats['Final Score'] + ' /' +
					' ' + matchStats.rounds[0].teams[1].team_stats['Final Score'] +
					' ' + matchStats.rounds[0].teams[1].team_stats.Team;
	
				ctx.fillText(headerTitle, 140, 50);
	
				// drawTable
				const rowMargin = 20;
				const rowWidth = 660;
				const rowHeight = 28;
				const rowTextTopPadding = 18;
				let ctxHeightPosition = 90;
				for (const team of matchStats.rounds[0].teams) {
	
					ctx.fillStyle = '#4d4d4d';
					ctx.fillRect(rowMargin, ctxHeightPosition, rowWidth, rowHeight);
					// team name
					ctx.font = 'bold 14px Play';
					ctx.fillStyle = '#EBEFF3';
					ctx.fillText(team.team_stats.Team, rowMargin + 4, ctxHeightPosition + rowTextTopPadding);
					// column names
					ctx.font = 'normal 14px Play';
					ctx.fillStyle = '#a0a0a0';
					ctx.fillText('Kills', rowMargin + 4 + 200, ctxHeightPosition + rowTextTopPadding);
					ctx.fillText('Assists', rowMargin + 4 + 250, ctxHeightPosition + rowTextTopPadding);
					ctx.fillText('Deaths', rowMargin + 4 + 320, ctxHeightPosition + rowTextTopPadding);
					ctx.fillText('K/D Ratio', rowMargin + 4 + 390, ctxHeightPosition + rowTextTopPadding);
					ctx.fillText('Headshots', rowMargin + 4 + 475, ctxHeightPosition + rowTextTopPadding);
					ctx.fillText('Headshots %', rowMargin + 4 + 563, ctxHeightPosition + rowTextTopPadding);
					ctxHeightPosition += rowHeight;
					// player Stats
					for (const [index, player] of team.players.entries()) {
						ctx.fillStyle = (index === 0 || !(index % 2)) ? '#0A0C0C' : '#141616';
						ctx.fillRect(rowMargin, ctxHeightPosition, rowWidth, rowHeight);
						ctx.fillStyle = '#EBEFF3';
						ctx.fillText(player.nickname, rowMargin + 4, ctxHeightPosition + rowTextTopPadding);
						ctx.fillText(player.player_stats.Kills, rowMargin + 4 + 200, ctxHeightPosition + rowTextTopPadding);
						ctx.fillText(player.player_stats.Assists, rowMargin + 4 + 250, ctxHeightPosition + rowTextTopPadding);
						ctx.fillText(player.player_stats.Deaths, rowMargin + 4 + 320, ctxHeightPosition + rowTextTopPadding);
						ctx.fillText(player.player_stats['K/D Ratio'], rowMargin + 4 + 390, ctxHeightPosition + rowTextTopPadding);
						ctx.fillText(player.player_stats.Headshot, rowMargin + 4 + 475, ctxHeightPosition + rowTextTopPadding);
						ctx.fillText(player.player_stats['Headshots %'], rowMargin + 4 + 563, ctxHeightPosition + rowTextTopPadding);
						ctxHeightPosition += rowHeight;
					}
					ctxHeightPosition += rowHeight;
				}
	
				const attachment = new Discord.MessageAttachment(canvas.toBuffer(), 'face-match-stats.png');
	
				// send msg to faceit channel
				const channel = client.channels.cache.get(config.discord_channel_id);
				channel.send(attachment);
	
			}
			catch (error) {
				console.log(util.inspect(error, { showHidden: false, depth: null }));
			}
	
		}
	
		res.end(';)');
	
	});
});


app.listen(process.env.PORT, () => {
	console.log(`Started on PORT ${process.env.PORT}`);
});

